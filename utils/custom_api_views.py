from rest_framework.views import APIView
from .jwtToken import JWT


class JWTAPIView(APIView):
    def __init__(self) -> None:
        self.jwt_ = JWT()

    def generate_token(
        self, user_id, two_factor_autenticated=False, permissions=tuple()
    ):
        token = self.jwt_.generate_jwt(
            {
                "user_id": user_id,
                "two_factor_autenticated": two_factor_autenticated,
                "permissions": permissions,
            }
        )
        return token
