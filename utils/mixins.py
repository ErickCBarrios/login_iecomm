from users.models import Users
from .jwtToken import JWT
from .exceptions import GeneralAPIException
from rest_framework import status


class TokenRequiredMixin:
    def __init__(self) -> None:
        self.jwt_ = JWT()

    def get_profile(self, request):
        headers = request.META
        try:
            payload = self.jwt_.decode_jwt(
                headers.get("HTTP_AUTHORIZATION"),
            )
        except Exception:
            raise GeneralAPIException("No has enviado un token o el token esta corrupto", 400)

        try:
            user = Users.objects.get(id=payload["user_id"])
        except Users.DoesNotExist:
            raise GeneralAPIException(
                detail="El usuario no existe",
                code=status.HTTP_404_NOT_FOUND,
            )
        except Exception:
            raise GeneralAPIException("El token enviado está corrupto o expiró", 400)

        return user
