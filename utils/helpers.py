import random
import string
from users.models import Users

from .pipes.send_email import send_email_pipe
from django.db import connections
from django.db.migrations.recorder import MigrationRecorder
from django.db.migrations.loader import MigrationLoader
from django.db.migrations.operations.fields import FieldOperation
from django.db.migrations.operations.models import ModelOperation, IndexOperation


def generate_random_code(numeric=True, alpha=True, length=4):
    if numeric and alpha:
        characters = string.ascii_letters + string.digits

    if numeric and not alpha:
        characters = string.digits

    if not numeric and alpha:
        characters = string.ascii_letters

    if not numeric and not alpha:
        characters = "0"

    suffix = "".join(random.choice(characters) for _ in range(length))
    return suffix



def send_db_changes(db_changes):
    db_change_report_template = (
        "action: {0}\ntable_name: {1}\ndescription: {2}\ndate: {3}\n"
    )

    db_changes_reports = [
        db_change_report_template.format(
            db_change.action,
            db_change.table_name,
            db_change.description,
            db_change.date,
        )
        for db_change in db_changes
    ]

    db_change_report = "\n".join(db_changes_reports)

    # TODO: Change this queryset to the real one on production
    # Here I send the info to the first created superuser, however this condition
    # may not be the real one
    user = Users.objects.filter(is_staff=True).first()

    subject = "Reporte de cambios en la base de datos"
    message = f"Las siguientes acciones fueron ejecutadas en la base de datos: \n{db_change_report}"
    to_email = [user.email]

    send_email_pipe(subject, message, to_email)


def get_user_permissions(user):
    permissions = set()

    user_groups = user.groups.all()
    for group in user_groups:
        group_permissions = group.permissions.all()
        for permission in group_permissions:
            permissions.add(permission.name)

    return tuple(permissions)


def get_migrations():
    loader = MigrationLoader(connections["default"])
    loader.load_disk()
    return loader.disk_migrations


def get_applied_migrations():
    recorder = MigrationRecorder(connections["default"])
    return recorder.applied_migrations()


def get_operation_info(operation):
    table_name = "No table found"
    action = "No action found"

    operation_class = type(operation)

    if issubclass(operation_class, FieldOperation):  # Field migration
        table_name = operation.model_name
        action = f"To field: {operation.name}"

    if issubclass(operation_class, (ModelOperation, IndexOperation)):  # Model migration
        table_name = operation.name
        action = f"To table: {table_name}"

    operation_class = type(operation)
    description = operation_class.__doc__

    return {
        "table_name": table_name,
        "description": description,
        "action": action,
    }


def remove_escape_sequences(string_with_escapes):
    clean_string = string_with_escapes.replace("\n", "").replace("\t", "").strip()
    return clean_string



