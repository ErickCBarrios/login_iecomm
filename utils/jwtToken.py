import jwt
from django.conf import settings
from .constants import *
import enum
from datetime import datetime, timedelta
import pytz


class TokenStatus(enum.Enum):
    valid = "Valid"
    expired = "Expired"     
    invalid = "Invalid"


class JWT:
    def generate_jwt(self, data: dict) -> str:
        exp = datetime.now(tz=pytz.UTC) + timedelta(days=1)
        payload = {
            "exp": exp,
            **data,
        }
        encoded_jwt = jwt.encode(
            payload,
            settings.JWT_SECRET_KEY,
            algorithm="HS256",
        )
        return encoded_jwt

    def decode_jwt(self, data: str):
        try:
            decoded_jwt = jwt.decode(
                data,
                settings.JWT_SECRET_KEY,
                algorithms=["HS256"],
                options={"verify_signature": True},
            )
            return decoded_jwt
        except jwt.ExpiredSignatureError:
            return TokenStatus.expired
        except jwt.InvalidSignatureError:
            return TokenStatus.invalid
