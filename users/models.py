from django.db import models
from django.contrib.auth.models import AbstractUser
from phonenumber_field.modelfields import PhoneNumberField
# Create your models here.
class Users(AbstractUser):
    phone_number = PhoneNumberField(
        verbose_name="Celular", null=True, blank=False, unique=True
    )
    profile_photo = models.ImageField(
        verbose_name="Foto de perfil",
        upload_to="profile_pictures",
        null=True,
        blank=True,
    )

    email = models.EmailField(unique=True)

    new_password = models.CharField(max_length=128, blank=True, null=True)

    class Meta:
        verbose_name = "Usuario"
        verbose_name_plural = "Usuarios"

    def __str__(self):
        return self.username if self.username else str(self.pk)
