from django.contrib import admin
from django.contrib.admin import ModelAdmin, register

from .models import Users


@register(Users)
class UsersAdmin(ModelAdmin):
    list_display = [
        "id",
        "username",
        "email",
        "first_name",
        "last_name",
        "is_active",
    ]
    list_filter = ["is_active"]
    search_fields = ["email", "username"]
    icon_name = "person"
