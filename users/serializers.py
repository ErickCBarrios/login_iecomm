from rest_framework import serializers
from .models import Users

class ListUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ["id", "username","first_name", "last_name", "email", "phone_number","password"]