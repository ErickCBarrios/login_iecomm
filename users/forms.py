from django import forms

import re

def validate_password(new_password):
    if len(new_password) < 8:
        raise forms.ValidationError("La contraseña debe tener al menos 8 caracteres.")

    if not re.search(r"\d", new_password):
        raise forms.ValidationError("La contraseña debe contener al menos un número.")

    if not re.search(r"[A-Za-z]", new_password):
        raise forms.ValidationError("La contraseña debe contener al menos una letra.")

    if not re.search(r'[!@#$%^&*(),.?":{}|<>_]', new_password):
        raise forms.ValidationError(
            "La contraseña debe contener al menos un carácter especial."
        )