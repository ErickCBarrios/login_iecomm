from django.urls import path

from .views import (

    AuthenticateUserAPIView,
    ChangePasswordAPIView,
    CreateUsersAPIView,
    ListUsersAPIView,
    UpdateUserAPIView,
    DeleteUserAPIView,
    UserDetailsAPIView,
    
)


urlpatterns = [
    path('list/', ListUsersAPIView.as_view(), name='list-users'),  # Listar usuarios
    path('create/', CreateUsersAPIView.as_view(), name='create-user'),  # Crear usuario
    path('detail/<int:pk>/', UserDetailsAPIView.as_view(), name='user-details'),  # Detalles de usuario
    path('update/<int:pk>/', UpdateUserAPIView.as_view(), name='update-user'),  # Actualizar usuario
    path('delete/<int:pk>/', DeleteUserAPIView.as_view(), name='delete-user'),  # Eliminar usuario     

    path("authenticate/", AuthenticateUserAPIView.as_view(), name="login"), #logear usuario
    path("change_password/", ChangePasswordAPIView.as_view(), name="change_password"), #Cambiar contrasenia
]
