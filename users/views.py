from utils.mixins import TokenRequiredMixin
from rest_framework.views import APIView
from .serializers import ListUserSerializer
from .models import Users
from rest_framework.response import Response
from utils.custom_api_views import JWTAPIView
from rest_framework.exceptions import AuthenticationFailed
from utils.exceptions import GeneralAPIException
from rest_framework import status, generics
from django.contrib.auth.hashers import make_password
from .forms import validate_password
from django.core.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated

class ListUsersAPIView(generics.ListAPIView):
    serializer_class = ListUserSerializer
    queryset = Users.objects.all()
    

class CreateUsersAPIView(generics.CreateAPIView):
    serializer_class = ListUserSerializer
    queryset = Users.objects.all()
    
    def perform_create(self, serializer):
            # Accede a los datos del objeto Users que se va a crear
        user_data = serializer.validated_data

        # Encripta la contraseña antes de guardarla en la base de datos
        user_data['password'] = make_password(user_data['password'])

        # Llama al método perform_create del padre para completar la creación del objeto
        super().perform_create(serializer)
        
        


class DeleteUserAPIView(generics.DestroyAPIView):
    queryset = Users.objects.all()
    serializer_class = ListUserSerializer

    def get_object(self):
        # Sobreescribe el método get_object para obtener el objeto a eliminar
        user_id = self.kwargs['pk']  # Obtén el ID del usuario de la URL
        return self.queryset.get(pk=user_id)
    
    

class UpdateUserAPIView(generics.UpdateAPIView):
    queryset = Users.objects.all()
    serializer_class = ListUserSerializer


    def perform_update(self, serializer):
        # Accede a los datos del objeto Users que se va a actualizar
        user_data = serializer.validated_data

        # Encripta la contraseña antes de actualizarla en la base de datos
        if 'password' in user_data:
            user_data['password'] = make_password(user_data['password'])

        # Llama al método perform_update del padre para completar la actualización del objeto
        super().perform_update(serializer)
        


class UserDetailsAPIView(generics.RetrieveAPIView):
    queryset = Users.objects.all()
    serializer_class = ListUserSerializer

    def get_object(self):
        # Sobreescribe el método get_object para obtener el objeto de detalle
        user_id = self.kwargs['pk']  # Obtén el ID del usuario de la URL
        return self.queryset.get(pk=user_id)


class AuthenticateUserAPIView(JWTAPIView):
    def post(self, request):
        user = self.get_object()

        if not user.check_password(request.POST.get("password")):
            raise AuthenticationFailed("Usuario o contraseña inválidos")

        response = {    
            "token": self.generate_token(
                user_id=user.id,
            ),
            "user": user.username,
        }

        return Response(response)

    def get_object(self):
        if not "username" in self.request.POST or not "password" in self.request.POST:
            raise GeneralAPIException(
                detail={"error": "El nombre de usuario y la contraseña son requeridos"},
                code=status.HTTP_400_BAD_REQUEST,
            )
        try:
            username = self.request.POST.get("username")
            return Users.objects.get(username=username)
        except Users.DoesNotExist:
            raise GeneralAPIException(
                detail="Usuario no existe", code=status.HTTP_404_NOT_FOUND
            )
            
class ChangePasswordAPIView(TokenRequiredMixin, APIView):
    def put(self, request):
        user = self.get_object()
        new_password = request.data.get("new_password")

        if not new_password:
            return Response(
                {"detail": "El campo contraseña es obligatorio."},
                status=status.HTTP_400_BAD_REQUEST,
            )
        try:
            validate_password(new_password)
        except ValidationError as e:
            return Response(
                {"detail": str(e).strip("[]").replace("'", "")},
                status=status.HTTP_400_BAD_REQUEST,
            )

        user = Users.objects.get(id=user.id)
        user.password = make_password(new_password)
        user.save()

        return Response(
            {"detail": "Contraseña cambiada exitosamente."},
            status=status.HTTP_200_OK,
        )

    def get_object(self):
        return self.get_profile(self.request)